<?php

namespace App\Entity;

use App\Repository\ContentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContentRepository::class)]
class Content
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        options: ['comment' => 'Unique Identifier']
    )]
    private ?int $id = null;

    /**
     * @var string|null Article name
     */
    #[ORM\Column(
        length: 255,
        options: ['comment' => 'Article name']
    )]
    private ?string $name = null;

    /**
     * @var string|null Article description
     */
    #[ORM\Column(
        type: 'text',
        nullable: true,
        options: ['comment' => 'Article description']
    )]
    private ?string $description = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908 and https://stackoverflow.com/questions/31056000/symfony-doctrine-timestamp-field.
     */
    #[ORM\Column(
        type: 'datetime',
        options: ['default' => 'CURRENT_TIMESTAMP', 'comment' => 'Insert date']
    )]
    private ?\DateTimeInterface $insert_date = null;

    #[ORM\Column(
        options: ['comment' => 'Insert by']
    )]
    private ?int $insert_by = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908
     */
    #[ORM\Column(
        type: 'datetime',
        nullable: true,
        options: ['default' => null, 'on update' => 'CURRENT_TIMESTAMP', 'comment' => 'Update date']
    )]
    private ?\DateTimeInterface $update_date = null;

    #[ORM\Column(
        nullable: true,
        options: ['comment' => 'Update by']
    )]
    private ?int $update_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getInsertDate(): ?\DateTimeInterface
    {
        return $this->insert_date;
    }

    public function getInsertBy(): ?int
    {
        return $this->insert_by;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->update_date;
    }

    public function getUpdateBy(): ?int
    {
        return $this->update_by;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function setInsertDate(\DateTimeInterface $insert_date): static
    {
        $this->insert_date = $insert_date;

        return $this;
    }

    public function setInsertBy(int $insert_by): static
    {
        $this->insert_by = $insert_by;

        return $this;
    }

    public function setUpdateDate(\DateTimeInterface|null $update_date): static
    {
        $this->update_date = $update_date;

        return $this;
    }

    public function setUpdateBy(int|null $update_by): static
    {
        $this->update_by = $update_by;

        return $this;
    }
}
