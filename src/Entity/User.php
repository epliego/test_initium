<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_EMAIL', fields: ['email'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        options: ['comment' => 'Unique Identifier']
    )]
    private ?int $id = null;

    /*
     * @var string|null The user email
     */
    #[ORM\Column(
        length: 180,
        options: ['comment' => 'User email']
    )]
    private ?string $email = null;

    /**
     * @var string|null The username
     */
    #[ORM\Column(
        length: 255,
        options: ['comment' => 'User name and last name']
    )]
    private ?string $name = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column(
        type: 'json',
        options: ['comment' => 'Users Roles']
    )]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(
        length: 255,
        options: ['comment' => 'Hashed password']
    )]
    private ?string $password = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908 and https://stackoverflow.com/questions/31056000/symfony-doctrine-timestamp-field
     * @var DateTimeInterface|null
     */
    #[ORM\Column(
        type: 'datetime',
        options: ['default' => 'CURRENT_TIMESTAMP', 'comment' => 'Insert date']
    )]
    private ?\DateTimeInterface $insert_date = null;

    #[ORM\Column(
        options: ['comment' => 'Insert by']
    )]
    private ?int $insert_by = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908
     */
    #[ORM\Column(
        type: 'datetime',
        nullable: true,
        options: ['default' => null, 'on update' => 'CURRENT_TIMESTAMP', 'comment' => 'Update date']
    )]
    private ?\DateTimeInterface $update_date = null;

    #[ORM\Column(
        nullable: true,
        options: ['comment' => 'Update by']
    )]
    private ?int $update_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getInsertDate(): ?DateTimeInterface
    {
        return $this->insert_date;
    }

    public function getInsertBy(): ?int
    {
        return $this->insert_by;
    }

    public function getUpdateDate(): ?DateTimeInterface
    {
        return $this->update_date;
    }

    public function getUpdateBy(): ?int
    {
        return $this->update_by;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function setInsertDate(DateTimeInterface $insert_date): static
    {
        $this->insert_date = $insert_date;

        return $this;
    }

    public function setInsertBy(int $insert_by): static
    {
        $this->insert_by = $insert_by;

        return $this;
    }

    public function setUpdateDate(DateTimeInterface|null $update_date): static
    {
        $this->update_date = $update_date;

        return $this;
    }

    public function setUpdateBy(int|null $update_by): static
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
