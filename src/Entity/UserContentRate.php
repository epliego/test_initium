<?php

namespace App\Entity;

use App\Repository\UserContentRateRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserContentRateRepository::class)]
class UserContentRate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        options: ['comment' => 'Unique Identifier']
    )]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false, options: ['comment' => 'User ID'])]
    private ?User $user = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false, options: ['comment' => 'Content ID'])]
    private ?Content $content = null;

    #[ORM\Column(
        options: ['comment' => 'Rate Content by User']
    )]
    private ?int $rate = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908 and https://stackoverflow.com/questions/31056000/symfony-doctrine-timestamp-field.
     */
    #[ORM\Column(
        type: 'datetime',
        options: ['default' => 'CURRENT_TIMESTAMP', 'comment' => 'Insert date']
    )]
    private ?\DateTimeInterface $insert_date = null;

    #[ORM\Column(
        options: ['comment' => 'Insert by']
    )]
    private ?int $insert_by = null;

    /**
     * Consulted (07-2024) in: https://antonlytvynov.medium.com/symfony-perfect-entity-dates-and-data-14f5d2733908
     */
    #[ORM\Column(
        type: 'datetime',
        nullable: true,
        options: ['default' => null, 'on update' => 'CURRENT_TIMESTAMP', 'comment' => 'Update date']
    )]
    private ?\DateTimeInterface $update_date = null;

    #[ORM\Column(
        nullable: true,
        options: ['comment' => 'Update by']
    )]
    private ?int $update_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getContent(): ?Content
    {
        return $this->content;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function getInsertDate(): ?\DateTimeInterface
    {
        return $this->insert_date;
    }

    public function getInsertBy(): ?int
    {
        return $this->insert_by;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->update_date;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setContent(Content $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function setRate(int $rate): static
    {
        $this->rate = $rate;

        return $this;
    }

    public function setInsertDate(\DateTimeInterface $insert_date): static
    {
        $this->insert_date = $insert_date;

        return $this;
    }

    public function setInsertBy(int $insert_by): static
    {
        $this->insert_by = $insert_by;

        return $this;
    }

    public function setUpdateDate(\DateTimeInterface|null $update_date): static
    {
        $this->update_date = $update_date;

        return $this;
    }

    public function setUpdateBy(int|null $update_by): static
    {
        $this->update_by = $update_by;

        return $this;
    }
}
