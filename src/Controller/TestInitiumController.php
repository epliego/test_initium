<?php

namespace App\Controller;

use App\Service\ContentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\UserService;
use App\Dto\Request\UserLoginRequestDto;
use App\Dto\Request\UserRegisterRequestDto;
use App\Dto\Request\ContentCreateRequestDto;
use App\Dto\Request\RateContentRequestDto;
use App\Dto\Request\FavoriteContentRequestDto;

#[Route('/api', name: 'api_')]
class TestInitiumController extends AbstractController
{
    private JWTEncoderInterface $jwtEncoder;

    public function __construct(JWTEncoderInterface $jwtEncoder){
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * User login
     * @param EntityManagerInterface $doctrine
     * @param UserLoginRequestDto $request
     * @param UserPasswordHasherInterface $password_hasher
     * @return JsonResponse
     */
    #[Route('/login', name: 'login', methods: 'post')]
    public function userLogin(EntityManagerInterface $doctrine, #[MapRequestPayload] UserLoginRequestDto $request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        // Consulted (07-2024) in: https://www.binaryboxtuts.com/php-tutorials/symfony-7-json-web-tokenjwt-authentication/ and https://vabadus.es/blog/symfony/autenticacion-jwt-en-symfony
        $user_service = new UserService($this->jwtEncoder);
        return $user_service->userLoginService($doctrine, $request, $password_hasher);
    }

    /**
     * User register
     * @param ManagerRegistry $doctrine
     * @param UserRegisterRequestDto $request
     * @param UserPasswordHasherInterface $password_hasher
     * @return JsonResponse
     */
    #[Route('/register', name: 'register', methods: 'post')]
    public function userRegister(ManagerRegistry $doctrine, #[MapRequestPayload] UserRegisterRequestDto $request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        // Consulted (07-2024) in: https://www.binaryboxtuts.com/php-tutorials/symfony-7-json-web-tokenjwt-authentication/
        $user_service = new UserService($this->jwtEncoder);
        return $user_service->userRegisterService($doctrine, $request, $password_hasher);
    }

    /**
     * Get user entity
     * @param Request $request
     * @param EntityManagerInterface $doctrine
     * @return JsonResponse
     */
    #[Route('/user', name: 'get_user', methods: 'get')]
    public function getUserEntity(Request $request, EntityManagerInterface $doctrine): JsonResponse
    {
        $user_service = new UserService($this->jwtEncoder);
        return $user_service->getUserEntityService($request, $doctrine);
    }

    /**
     * Update user entity
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    #[Route('/user', name: 'put_user', methods: 'put')]
    public function updateUser(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] UserRegisterRequestDto $user_request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        $user_service = new UserService($this->jwtEncoder);
        return $user_service->updateUserService($doctrine, $request, $user_request, $password_hasher);
    }

    /**
     * Create a new content
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param ContentCreateRequestDto $request_content
     * @return JsonResponse
     */
    #[Route('/content', name: 'content_create', methods: 'post')]
    public function createContent(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] ContentCreateRequestDto $request_content): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->createContentService($doctrine, $request, $request_content);
    }

    /**
     * Get content favorites by user
     * @param EntityManagerInterface $doctrine
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/content/favorites', name: 'get_content_favorites', methods: 'get')]
    public function getContentFavorites(EntityManagerInterface $doctrine, Request $request): JsonResponse
    {
        // Consulted (07-2024) in: https://symfonycasts.com/screencast/symfony7-upgrade/request-data
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->getContentFavoritesService($doctrine, $request);
    }

    /**
     * Get content by id
     * @param EntityManagerInterface $doctrine
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/content/{id}', name: 'get_content_id', methods: 'get')]
    public function getContentById(EntityManagerInterface $doctrine, int $id): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->getContentByIdService($doctrine, $id);
    }

    /**
     * Update a content
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param ContentCreateRequestDto $request_content
     * @return JsonResponse
     */
    #[Route('/content/{id}', name: 'content_update', methods: 'put')]
    public function updateContent(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] ContentCreateRequestDto $request_content, int $id): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->updateContentService($doctrine, $request, $request_content, $id);
    }

    /**
     * Delete content by id
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/content/{id}', name: 'delete_content_id', methods: 'delete')]
    public function deleteContentById(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->deleteContentByIdService($doctrine, $id);
    }

    /**
     * Get content by filter
     * @param EntityManagerInterface $doctrine
     * @param string $filter
     * @return JsonResponse
     */
    #[Route('/content', name: 'get_content_filter', methods: 'get')]
    public function getContentByFilter(EntityManagerInterface $doctrine, #[MapQueryParameter] string $filter = ''): JsonResponse
    {
        // Consulted (07-2024) in: https://symfonycasts.com/screencast/symfony7-upgrade/request-data
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->getContentByFilterService($doctrine, $filter);
    }

    /**
     * Rate a content by user
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param RateContentRequestDto $request_rate_content
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/content/{id}/rate', name: 'rate_content_user', methods: 'post')]
    public function rateContent(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] RateContentRequestDto $request_rate_content, int $id): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->rateContentService($doctrine, $request, $request_rate_content, $id);
    }

    /**
     * Favorite content by user
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param FavoriteContentRequestDto $request_favorite_content
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/content/{id}/favorite', name: 'favorite_content_user', methods: 'post')]
    public function favoriteContent(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] FavoriteContentRequestDto $request_favorite_content, int $id): JsonResponse
    {
        $content_service = new ContentService($this->jwtEncoder);
        return $content_service->favoriteContentService($doctrine, $request, $request_favorite_content, $id);
    }
}
