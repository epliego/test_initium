<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Dto\Request\UserLoginRequestDto;
use App\Dto\Request\UserRegisterRequestDto;

class UserService
{
    private JWTEncoderInterface $jwtEncoder;

    public function __construct(JWTEncoderInterface $jwtEncoder){
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * Create a JWT access token for the user
     * @param User $user
     * @return JsonResponse
     */
    private function createTokenService(User $user): JsonResponse
    {
        try {
            $token = $this->jwtEncoder->encode([
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'username' => $user->getName(),
                'roles' => $user->getRoles(),
                'exp' => time() + 3600, // 1 hour expiration
            ]);

            $array_response = [
                'status' => 200,
                'message' => ['User Login Successfully'],
                'data' => [
                    'access_token' => $token,
                ]
            ];

            return new JsonResponse($array_response, 200);
        } catch (JWTEncodeFailureException $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
            // throw new \RuntimeException('Error while encoding token');
        }
    }

    /**
     * Decode a JWT access token
     * @param Request $request
     * @return array
     */
    public function decodeTokenService(Request $request): array
    {
        try {
            $token = str_replace("Bearer ", "", $request->headers->get('Authorization'));

            return $this->jwtEncoder->decode($token);
        } catch (JWTDecodeFailureException $e) {
            throw new \InvalidArgumentException('Invalid token');
        }
    }

    /**
     * Login a user
     * @param EntityManagerInterface $doctrine
     * @param UserLoginRequestDto $request
     * @param UserPasswordHasherInterface $password_hasher
     * @return JsonResponse
     */
    public function userLoginService(EntityManagerInterface $doctrine, #[MapRequestPayload] UserLoginRequestDto $request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        try {
            $array_message_errors = [];

            // $decoded = json_decode($request->getContent());
            $email = $request->email;
            $password = $request->password;

            $user = $doctrine->getRepository(User::class)
                ->findOneBy(['email' => $email]);

            if (!$user) {
                $array_message_errors[] = 'Invalid email';
            }elseif (!$password_hasher->isPasswordValid($user, $password)) {
                $array_message_errors[] = 'Invalid password';
            }

            if (count($array_message_errors) > 0) {
                return new JsonResponse([
                    'status' => 400,
                    'message' => $array_message_errors,
                    'data' => []
                ], 400);
            } else {
                return $this->createTokenService($user);
            }
        }  catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Register a user
     * @param ManagerRegistry $doctrine
     * @param UserRegisterRequestDto $request
     * @param UserPasswordHasherInterface $password_hasher
     * @return JsonResponse
     */
    public function userRegisterService(ManagerRegistry $doctrine, #[MapRequestPayload] UserRegisterRequestDto $request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        try {
            $em = $doctrine->getManager();
            // $decoded = json_decode($request->getContent());
            $email = $request->email;
            $plain_text_password = $request->password;

            $user = $doctrine->getRepository(User::class)
                ->findOneBy(['email' => $email]);

            if ($user) {
                $array_response = [
                    'status' => 400,
                    'message' => ['User already exists'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $user = new User();
                $hashed_password = $password_hasher->hashPassword(
                    $user,
                    $plain_text_password
                );
                $user->setPassword($hashed_password);
                $user->setEmail($email);
                $user->setName($request->name);
                $user->setInsertDate(new \DateTime());
                $user->setInsertBy(1);
                $em->persist($user);
                $em->flush();

                $user->setInsertBy($user->getId());
                $user->setUpdateDate(new \DateTime());
                $em->persist($user);
                $em->flush();

                $array_response = [
                    'status' => 201,
                    'message' => ['User Registered Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 201);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Get the user entity
     * @param Request $request
     * @param EntityManagerInterface $doctrine
     * @return JsonResponse
     */
    public function getUserEntityService(Request $request, EntityManagerInterface $doctrine): JsonResponse
    {
        try {
            $access_token = $this->decodeTokenService($request);

            $user = $doctrine->getRepository(User::class)
                ->findOneBy(['id' => $access_token['id']]);

            if (!$user) {
                throw new \InvalidArgumentException('Invalid token');
            }

            $array_response = [
                'status' => 200,
                'message' => ['User Found'],
                'data' => [
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'name' => $user->getName(),
                    'roles' => $user->getRoles(),
                ]
            ];

            return new JsonResponse($array_response, 200);
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Update the user entity
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param UserRegisterRequestDto $user_request
     * @param UserPasswordHasherInterface $password_hasher
     * @return JsonResponse
     */
    public function updateUserService(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] UserRegisterRequestDto $user_request, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        try {
            $access_token = $this->decodeTokenService($request);

            $user = $doctrine->getRepository(User::class)
                ->findOneBy(['id' => $access_token['id']]);

            if (!$user) {
                throw new \InvalidArgumentException('Invalid token');
            }

            $em = $doctrine->getManager();
            // $decoded = json_decode($request->getContent());
            $email = $user_request->email;
            $plain_text_password = $user_request->password;

            $user_repository = $doctrine->getRepository(User::class);

            $query_builder = $user_repository->createQueryBuilder('u') // Consulted (07-2024) in: https://symfony.com/doc/current/doctrine.html#querying-with-the-query-builder
                ->where('u.email = :email')
                ->andWhere('u.id != :user_id')
                ->setParameter('email', $email)
                ->setParameter('user_id', $user->getId());

            $query = $query_builder->getQuery();

            if (count($query->getResult()) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => ['Email already exists'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $hashed_password = $password_hasher->hashPassword(
                    $user,
                    $plain_text_password
                );
                $user->setPassword($hashed_password);
                $user->setEmail($email);
                $user->setName($user_request->name);
                $user->setUpdateDate(new \DateTime());
                $user->setUpdateBy($user->getId());
                $em->persist($user);
                $em->flush();

                $array_response = [
                    'status' => 200,
                    'message' => ['User Updated Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 200);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }
}
