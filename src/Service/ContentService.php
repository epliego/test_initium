<?php

namespace App\Service;

use App\Dto\Request\FavoriteContentRequestDto;
use App\Entity\Content;
use App\Entity\User;
use App\Entity\UserContentFavorite;
use App\Entity\UserContentRate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Dto\Request\ContentCreateRequestDto;
use App\Dto\Request\RateContentRequestDto;

class ContentService
{
    private JWTEncoderInterface $jwtEncoder;

    public function __construct(JWTEncoderInterface $jwtEncoder){
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * Create a new content
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param ContentCreateRequestDto $request_content
     * @return JsonResponse
     */
    public function createContentService(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] ContentCreateRequestDto $request_content): JsonResponse
    {
        try {
            $user_service = new UserService($this->jwtEncoder);

            $access_token = $user_service->decodeTokenService($request);

            $content = new Content();

            $content->setName($request_content->name);
            $content->setDescription($request_content->description);
            $content->setInsertDate(new \DateTime());
            $content->setInsertBy($access_token['id']);

            $em = $doctrine->getManager();
            $em->persist($content);
            $em->flush();

            $array_response = [
                'status' => 201,
                'message' => ['Content Created Successfully'],
                'data' => []
            ];

            return new JsonResponse($array_response, 201);
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Get content by id
     * @param EntityManagerInterface $doctrine
     * @param int $id
     * @return JsonResponse
     */
    public function getContentByIdService(EntityManagerInterface $doctrine, int $id): JsonResponse
    {
        try {
            $array_errors = [];

            $content = $doctrine->getRepository(Content::class)
                ->findOneBy(['id' => $id]);

            if (!$content) {
                $array_errors[] = 'No Content Found';
            }

            if (count($array_errors) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => $array_errors,
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $array_response = [
                    'status' => 200,
                    'message' => ['Content Found Successfully'],
                    'data' => [
                        'id' => $content->getId(),
                        'name' => $content->getName(),
                        'description' => $content->getDescription(),
                    ]
                ];

                return new JsonResponse($array_response, 200);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Update a content
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param ContentCreateRequestDto $request_content
     * @param int $id
     * @return JsonResponse
     */
    public function updateContentService(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] ContentCreateRequestDto $request_content, int $id): JsonResponse
    {
        try {
            $array_errors = [];

            $user_service = new UserService($this->jwtEncoder);

            $access_token = $user_service->decodeTokenService($request);

            $content = $doctrine->getRepository(Content::class)
                ->findOneBy(['id' => $id]);

            if (!$content) {
                $array_errors[] = 'No Content Found';
            }

            if (count($array_errors) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => $array_errors,
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $content->setName($request_content->name);
                $content->setDescription($request_content->description);
                $content->setUpdateDate(new \DateTime());
                $content->setUpdateBy($access_token['id']);

                $em = $doctrine->getManager();
                $em->persist($content);
                $em->flush();

                $array_response = [
                    'status' => 200,
                    'message' => ['Content Updated Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 200);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Delete content by id
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return JsonResponse
     */
    public function deleteContentByIdService(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        try {
            $array_errors = [];

            $content = $doctrine->getRepository(Content::class)
                ->findOneBy(['id' => $id]);

            if (!$content) {
                $array_errors[] = 'No Content Found';
            }

            if (count($array_errors) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => $array_errors,
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $em = $doctrine->getManager();
                $em->remove($content);
                $em->flush();

                $array_response = [
                    'status' => 200,
                    'message' => ['Content Deleted Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 200);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Get content by filter
     * @param EntityManagerInterface $doctrine
     * @param string $filter
     * @return JsonResponse
     */
    public function getContentByFilterService(EntityManagerInterface $doctrine, #[MapQueryParameter] string $filter = ''): JsonResponse
    {
        try {
            $content_repository = $doctrine->getRepository(Content::class);

            $query_builder = $content_repository->createQueryBuilder('c')
                ->where('c.name LIKE :name')
                ->orWhere('c.description LIKE :description')
                ->setParameter('name', '%'.$filter.'%')
                ->setParameter('description', '%'.$filter.'%')
                ->getQuery()
                ->getResult(); // Consulted (07-2024) in: https://symfony.com/doc/current/doctrine.html#querying-with-the-query-builder and https://stackoverflow.com/questions/8164682/doctrine-and-like-query

            $array_content = [];
            foreach ($query_builder as $content) {
                $array_content[] = [
                    'id' => $content->getId(),
                    'name' => $content->getName(),
                    'description' => $content->getDescription(),
                ];
            }

            $array_response = [
                'status' => 200,
                'message' => ['Content Filtered Successfully'],
                'data' => $array_content
            ];

            return new JsonResponse($array_response, 200);
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Rate a content by user
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param RateContentRequestDto $request_rate_content
     * @param int $id
     * @return JsonResponse
     */
    public function rateContentService(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] RateContentRequestDto $request_rate_content, int $id): JsonResponse
    {
        try {
            $array_errors = [];

            if ($request_rate_content->rate < 1 || $request_rate_content->rate > 5) {
                $array_errors[] = 'Invalid Rate Value. Must be between 1 and 5';
            }

            $user_service = new UserService($this->jwtEncoder);

            $access_token = $user_service->decodeTokenService($request);

            $user_content_rate = $doctrine->getRepository(UserContentRate::class)
                ->findOneBy(['user' => $access_token['id'], 'content' => $id]);

            if ($user_content_rate) {
                $array_errors[] = 'User already rated this content';
            }

            $content = $doctrine->getRepository(Content::class)
                ->findOneBy(['id' => $id]);

            if (!$content) {
                $array_errors[] = 'No Content Found';
            }

            if (count($array_errors) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => $array_errors,
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $user = $doctrine->getRepository(User::class)
                    ->findOneBy(['id' => $access_token['id']]);

                $new_user_content_rate = new UserContentRate();

                $new_user_content_rate->setUser($user);
                $new_user_content_rate->setContent($content);
                $new_user_content_rate->setRate($request_rate_content->rate);
                $new_user_content_rate->setInsertDate(new \DateTime());
                $new_user_content_rate->setInsertBy($access_token['id']);

                $em = $doctrine->getManager();
                $em->persist($new_user_content_rate);
                $em->flush();

                $array_response = [
                    'status' => 201,
                    'message' => ['Content Rated by User Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 201);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Favorite content by user
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param FavoriteContentRequestDto $request_favorite_content
     * @param int $id
     * @return JsonResponse
     */
    public function favoriteContentService(ManagerRegistry $doctrine, Request $request, #[MapRequestPayload] FavoriteContentRequestDto $request_favorite_content, int $id): JsonResponse
    {
        try {
            $array_errors = [];

            if ($request_favorite_content->favorite != 1 && $request_favorite_content->favorite != 0) {
                $array_errors[] = 'Invalid Favorite Value. Must be 1 or 0';
            }

            $user_service = new UserService($this->jwtEncoder);

            $access_token = $user_service->decodeTokenService($request);

            $user_content_favorite = $doctrine->getRepository(UserContentFavorite::class)
                ->findOneBy(['user' => $access_token['id'], 'content' => $id]);

            if ($user_content_favorite) {
                $array_errors[] = 'User already marked this content like favorite';
            }

            $content = $doctrine->getRepository(Content::class)
                ->findOneBy(['id' => $id]);

            if (!$content) {
                $array_errors[] = 'No Content Found';
            }

            if (count($array_errors) > 0) {
                $array_response = [
                    'status' => 400,
                    'message' => $array_errors,
                    'data' => []
                ];

                return new JsonResponse($array_response, 400);
            } else {
                $user = $doctrine->getRepository(User::class)
                    ->findOneBy(['id' => $access_token['id']]);

                $new_user_content_favorite = new UserContentFavorite();

                $new_user_content_favorite->setUser($user);
                $new_user_content_favorite->setContent($content);
                $new_user_content_favorite->setFavorite($request_favorite_content->favorite);
                $new_user_content_favorite->setInsertDate(new \DateTime());
                $new_user_content_favorite->setInsertBy($access_token['id']);

                $em = $doctrine->getManager();
                $em->persist($new_user_content_favorite);
                $em->flush();

                $array_response = [
                    'status' => 201,
                    'message' => ['Content Marked like Favorite by User Successfully'],
                    'data' => []
                ];

                return new JsonResponse($array_response, 201);
            }
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }

    /**
     * Get content favorites by user
     * @param EntityManagerInterface $doctrine
     * @param Request $request
     * @return JsonResponse
     */
    public function getContentFavoritesService(EntityManagerInterface $doctrine, Request $request): JsonResponse
    {
        try {
            $user_service = new UserService($this->jwtEncoder);

            $access_token = $user_service->decodeTokenService($request);

            $user_content_favorite_repository = $doctrine->getRepository(UserContentFavorite::class);

            $query_builder = $user_content_favorite_repository->createQueryBuilder('cf')
                ->innerJoin('cf.content', 'c', 'WITH', 'cf.content = c.id')
                ->innerJoin('cf.user', 'u', 'WITH', 'cf.user = u.id')
                ->where('cf.user = :user')
                ->setParameter('user', $access_token['id'])
                ->getQuery()
                ->getResult(); // Consulted (07-2024) in: https://symfony.com/doc/current/doctrine.html#querying-with-the-query-builder and https://stackoverflow.com/questions/8164682/doctrine-and-like-query

            $array_user_content_favorite = [];
            foreach ($query_builder as $user_content_favorite) {
                $content = $user_content_favorite->getContent();

                $array_user_content_favorite[] = [
                    'id' => $content->getId(),
                    'name' => $content->getName(),
                    'description' => $content->getDescription(),
                ];
            }

            $array_response = [
                'status' => 200,
                'message' => ['User Content Favorite Consulted Successfully'],
                'data' => $array_user_content_favorite
            ];

            return new JsonResponse($array_response, 200);
        } catch (\Exception $exception) {
            $array_response = [
                'status' => 500,
                'message' => [$exception->getMessage()],
                'data' => []
            ];

            return new JsonResponse($array_response, 500);
        }
    }
}
