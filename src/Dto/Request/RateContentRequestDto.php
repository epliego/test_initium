<?php

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

// Consulted (07-2024) in: https://dev.to/daniyaljavani/a-simple-way-to-validate-api-requests-in-symfony-with-dtos-and-annotations-1ge0
readonly class RateContentRequestDto
{
    public function __construct(
        #[Assert\NotBlank(
            message: 'The rate should not be blank.',
        )]
        #[Assert\Type(
            type: 'int',
            message: 'The rate {{ value }} is not a valid rate.',
        )]
        public readonly int $rate,
    ){}
}
