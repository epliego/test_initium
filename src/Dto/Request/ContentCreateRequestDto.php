<?php

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

// Consulted (07-2024) in: https://dev.to/daniyaljavani/a-simple-way-to-validate-api-requests-in-symfony-with-dtos-and-annotations-1ge0
readonly class ContentCreateRequestDto
{
    public function __construct(
        #[Assert\NotBlank(
            message: 'The name should not be blank.',
        )]
        #[Assert\Type(
            type: 'string',
            message: 'The name {{ value }} is not a valid name.',
        )]
        public readonly string $name,

        #[Assert\NotBlank(
            message: 'The description should not be blank.',
        )]
        #[Assert\Type(
            type: 'string',
            message: 'The description {{ value }} is not a valid {{ type }}.',
        )]
        public readonly string $description,
    ){}
}
