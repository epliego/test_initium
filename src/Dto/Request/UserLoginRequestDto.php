<?php

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

// Consulted (07-2024) in: https://dev.to/daniyaljavani/a-simple-way-to-validate-api-requests-in-symfony-with-dtos-and-annotations-1ge0
readonly class UserLoginRequestDto
{
    public function __construct(
        #[Assert\NotBlank(
            message: 'The email should not be blank.',
        )]
        #[Assert\Email(
            message: 'The email {{ value }} is not a valid email.',
        )]
        public readonly string $email,

        #[Assert\NotBlank(
            message: 'The password should not be blank.',
        )]
        #[Assert\Type(
            type: 'string',
            message: 'The password {{ value }} is not a valid {{ type }}.',
        )]
        public readonly string $password,
    ){}
}
