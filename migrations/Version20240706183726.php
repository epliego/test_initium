<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240706183726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL COMMENT \'Unique Identifier\', name VARCHAR(255) NOT NULL COMMENT \'Article name\', description LONGTEXT DEFAULT NULL COMMENT \'Article description\', insert_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'Insert date\', insert_by INT NOT NULL COMMENT \'Insert by\', update_date DATETIME DEFAULT NULL COMMENT \'Update date\', update_by INT DEFAULT NULL COMMENT \'Update by\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL COMMENT \'Unique Identifier\', email VARCHAR(180) NOT NULL COMMENT \'User email\', name VARCHAR(255) NOT NULL COMMENT \'User name and last name\', roles JSON NOT NULL COMMENT \'Users Roles(DC2Type:json)\', password VARCHAR(255) NOT NULL COMMENT \'Hashed password\', insert_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'Insert date\', insert_by INT NOT NULL COMMENT \'Insert by\', update_date DATETIME DEFAULT NULL COMMENT \'Update date\', update_by INT DEFAULT NULL COMMENT \'Update by\', UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_content_favorite (id INT AUTO_INCREMENT NOT NULL COMMENT \'Unique Identifier\', user_id INT NOT NULL COMMENT \'User ID\', content_id INT NOT NULL COMMENT \'Content ID\', favorite INT NOT NULL COMMENT \'Favorite Content by User, 0 = not favorite, 1 = favorite\', insert_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'Insert date\', insert_by INT NOT NULL COMMENT \'Insert by\', update_date DATETIME DEFAULT NULL COMMENT \'Update date\', update_by INT DEFAULT NULL COMMENT \'Update by\', INDEX IDX_476726EBA76ED395 (user_id), INDEX IDX_476726EB84A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_content_rate (id INT AUTO_INCREMENT NOT NULL COMMENT \'Unique Identifier\', user_id INT NOT NULL COMMENT \'User ID\', content_id INT NOT NULL COMMENT \'Content ID\', rate INT NOT NULL COMMENT \'Rate Content by User\', insert_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'Insert date\', insert_by INT NOT NULL COMMENT \'Insert by\', update_date DATETIME DEFAULT NULL COMMENT \'Update date\', update_by INT DEFAULT NULL COMMENT \'Update by\', INDEX IDX_9242AEFEA76ED395 (user_id), INDEX IDX_9242AEFE84A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_content_favorite ADD CONSTRAINT FK_476726EBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_content_favorite ADD CONSTRAINT FK_476726EB84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE user_content_rate ADD CONSTRAINT FK_9242AEFEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_content_rate ADD CONSTRAINT FK_9242AEFE84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_content_favorite DROP FOREIGN KEY FK_476726EBA76ED395');
        $this->addSql('ALTER TABLE user_content_favorite DROP FOREIGN KEY FK_476726EB84A0A3ED');
        $this->addSql('ALTER TABLE user_content_rate DROP FOREIGN KEY FK_9242AEFEA76ED395');
        $this->addSql('ALTER TABLE user_content_rate DROP FOREIGN KEY FK_9242AEFE84A0A3ED');
        $this->addSql('DROP TABLE content');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_content_favorite');
        $this->addSql('DROP TABLE user_content_rate');
    }
}
